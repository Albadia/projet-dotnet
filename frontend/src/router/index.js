import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import Auth from "@okta/okta-vue";

Vue.use(Auth, {
  issuer: "https://dev-58395097.okta.com/oauth2/default",
  client_id: "0oa12ys37e1LEvKe95d7",
  redirect_uri: "https://localhost:8080/",
  scope: "openid profile email",
});

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/about",
    name: "About",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/About.vue"),
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
